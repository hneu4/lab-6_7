package com.workshop.lab6.web;

import com.workshop.lab6.exceptions.BankAccountNotFoundException;
import com.workshop.lab6.service.BankAccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BankAccountControllerTest {

    @Mock
    private BankAccountService bankAccountService;

    @InjectMocks
    private BankAccountController controller;

    @Test
    public void getBalanceSuccess(){
        Long accountId = 12L;
        when(bankAccountService.getAccountBalance(accountId)).thenReturn(10.0);
        double result = controller.getBankAccountBalance(accountId);
        assertTrue(result == 10);
    }

    @Test
    public void getBalanceSuccessIncorrecAccountId(){
        Long accountId = 12L;
        when(bankAccountService.getAccountBalance(accountId)).thenThrow(new BankAccountNotFoundException(accountId));
        assertThrows(BankAccountNotFoundException.class, ()-> controller.getBankAccountBalance(accountId));
    }
}
