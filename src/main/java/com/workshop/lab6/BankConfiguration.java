package com.workshop.lab6;

import com.workshop.lab6.web.BankAccountController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.modelmapper.ModelMapper;

@Configuration
public class BankConfiguration {

    @Bean
    public ModelMapper getMapper() {
        return new ModelMapper();
    }
}
