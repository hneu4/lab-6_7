package com.workshop.lab6.service;

import com.workshop.lab6.exceptions.BankAccountNotFoundException;
import com.workshop.lab6.exceptions.IncorrectAmountOfMoneyException;
import com.workshop.lab6.model.BankAccountDto;
import com.workshop.lab6.model.UpdateBalanceBody;
import com.workshop.lab6.model.entity.BankAccount;
import com.workshop.lab6.repository.BankAccountRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BankAccountService {
    private final BankAccountRepository bankAccountRepository;
    private final ModelMapper mapper;

    public BankAccountService(
            BankAccountRepository bankAccountRepository,
            ModelMapper mapper
    ) {
        this.bankAccountRepository = bankAccountRepository;
        this.mapper = mapper;
    }

    private BankAccount getAccount(Long accountId){
        Optional<BankAccount> bankAccount = bankAccountRepository.findById(accountId);
        if(bankAccount.isEmpty()) {
            throw new BankAccountNotFoundException(accountId);
        } else {
            return bankAccount.get();
        }
    }

    public double getAccountBalance(Long accountId){
        return getAccount(accountId).getBalance();
    }
    public BankAccountDto createAccount(BankAccountDto bankAccountDto){
        BankAccount bankAccount = mapper.map(bankAccountDto, BankAccount.class);
        BankAccount createdAccount = bankAccountRepository.save(bankAccount);
        return mapper.map(createdAccount, BankAccountDto.class);
    }
    public void updateBalance(UpdateBalanceBody body){
        BankAccount bankAccount = getAccount(body.getAccountId());
        double newBalance = bankAccount.getBalance() + body.getAmount();
        if(newBalance<0){
            throw new IncorrectAmountOfMoneyException();
        } else {
            bankAccount.setBalance(newBalance);
        }
        bankAccountRepository.save(bankAccount);
    }
}
