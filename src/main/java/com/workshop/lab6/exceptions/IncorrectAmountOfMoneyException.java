package com.workshop.lab6.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class IncorrectAmountOfMoneyException  extends RuntimeException{
}
