package com.workshop.lab6.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class BankAccountNotFoundException  extends RuntimeException{

    public BankAccountNotFoundException(Long id){
        super("Bank Account with id=" + id + " not found!");
    }
}
