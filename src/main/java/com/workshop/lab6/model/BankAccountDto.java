package com.workshop.lab6.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
public class BankAccountDto {
    @JsonProperty(access=JsonProperty.Access.READ_ONLY)
    private Long accountId;

    private String name;

    @NotNull
    private String surname;

    @NotNull
    @NotEmpty
    private String phoneNumber;

    @Min(value = 0)
    private double balance;
}
