package com.workshop.lab6.model.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name="accounts")
@Data
public class BankAccount {
    @Id
    @GeneratedValue
    @Column(name="account_id")
    private Long accountId;
    private String name;
    private String surname;
    private String phoneNumber;
    private double balance;
}
