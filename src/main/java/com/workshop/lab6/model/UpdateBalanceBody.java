package com.workshop.lab6.model;

import lombok.Data;

@Data
public class UpdateBalanceBody {
    private Long accountId;
    private double amount;
}
