package com.workshop.lab6.web;

import com.workshop.lab6.model.BankAccountDto;
import com.workshop.lab6.model.UpdateBalanceBody;
import com.workshop.lab6.service.BankAccountService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;


@RestController("/account")
public class BankAccountController {
    private final BankAccountService bankAccountService;

    public BankAccountController(BankAccountService service){
        this.bankAccountService = service;
    }

    @GetMapping("/{accountId}")
    public double getBankAccountBalance(@PathVariable Long accountId){

        return bankAccountService.getAccountBalance(accountId);
    }

    @PostMapping
    public BankAccountDto createAccount(@Valid @RequestBody BankAccountDto bankAccountDto){
        return bankAccountService.createAccount(bankAccountDto);
    }

    @PutMapping("/update/balance")
    public void updateBalance(@RequestBody UpdateBalanceBody body){
        bankAccountService.updateBalance(body);
    }
}
