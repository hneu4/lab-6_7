package com.workshop.lab6.repository;

import com.workshop.lab6.model.BankAccountDto;
import com.workshop.lab6.model.entity.BankAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankAccountRepository extends CrudRepository<BankAccount, Long> {
}
